package cat.itb.list_grid_view.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.MenuView;

import android.content.ClipData;
import android.content.Intent;
import android.media.audiofx.DynamicsProcessing;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.zip.Inflater;

import cat.itb.list_grid_view.Adapters.MyAdapter;
import cat.itb.list_grid_view.Models.EqProfile;
import cat.itb.list_grid_view.R;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    GridView gridView;
    ArrayList<EqProfile> profiles;
    MyAdapter myAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);
        gridView = (GridView) findViewById(R.id.gridView);

        profiles = new ArrayList<EqProfile>() {{
            add(new EqProfile(R.drawable.jazz, 40, 10, 30, "Jazz"));
            add(new EqProfile(R.drawable.pop, 30, 50, 30, "Pop"));
            add(new EqProfile(R.drawable.rock, 20, 10, 40, "Rock"));
            add(new EqProfile(R.drawable.classical, 35, 40, 20, "Classical"));
            add(new EqProfile(R.drawable.rap, 40, 15, 20, "Rap"));
        }};

        myAdapter = new MyAdapter(this, R.layout.list_item, profiles);
        listView.setAdapter(myAdapter);
        gridView.setAdapter(myAdapter);

        registerForContextMenu(listView);
        registerForContextMenu(gridView);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            int icon = 0;
            int iconBundle = (int) bundle.get("icon");

            switch (iconBundle) {
                case 0:
                    icon = R.drawable.jazz;
                    profiles.add(new EqProfile(icon, bundle.getInt("low"), bundle.getInt("mid"),
                            bundle.getInt("high"), bundle.getString("name")));
                    break;
                case 1:
                    icon = R.drawable.rock;
                    profiles.add(new EqProfile(icon, bundle.getInt("low"), bundle.getInt("mid"),
                            bundle.getInt("high"), bundle.getString("name")));
                    break;
                case 2:
                    icon = R.drawable.pop;
                    profiles.add(new EqProfile(icon, bundle.getInt("low"), bundle.getInt("mid"),
                            bundle.getInt("high"), bundle.getString("name")));
                    break;
                case 3:
                    icon = R.drawable.classical;
                    profiles.add(new EqProfile(icon, bundle.getInt("low"), bundle.getInt("mid"),
                            bundle.getInt("high"), bundle.getString("name")));
                    break;
                case 4:
                    icon = R.drawable.rap;
                    profiles.add(new EqProfile(icon, bundle.getInt("low"), bundle.getInt("mid"),
                            bundle.getInt("high"), bundle.getString("name")));
                    break;
                case 5:
                    profiles.get(bundle.getInt("position")).setName(bundle.getString("name"));
                    profiles.get(bundle.getInt("position")).setLowPass(bundle.getInt("low"));
                    profiles.get(bundle.getInt("position")).setMidPass(bundle.getInt("mid"));
                    profiles.get(bundle.getInt("position")).setHighPass(bundle.getInt("high"));
                    myAdapter.notifyDataSetChanged();
            }

        }
        myAdapter = new MyAdapter(this, R.layout.list_item, profiles);
        listView.setAdapter(myAdapter);
        gridView.setAdapter(myAdapter);

        myAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addButton:
                Intent intent = new Intent(MainActivity.this, AddActivity.class);
                startActivity(intent);
                return true;
            case R.id.gridViewButton:
                gridView.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
                return true;
            case R.id.listViewButton:
                gridView.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
                return true;
        }
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle(this.profiles.get(info.position).toString());

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId()) {
            case R.id.delete_item:
                profiles.remove(info.position);
                myAdapter.notifyDataSetChanged();
                return true;
            case R.id.edit_item:
                Intent intent = new Intent(MainActivity.this, CheckProfileActivity.class);
                intent.putExtra("name", profiles.get(info.position).toString());
                intent.putExtra("low", profiles.get(info.position).getLowPass());
                intent.putExtra("mid", profiles.get(info.position).getMidPass());
                intent.putExtra("high", profiles.get(info.position).getHighPass());
                intent.putExtra("position", info.position);
                startActivity(intent);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}