package cat.itb.list_grid_view.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;

import androidx.appcompat.app.AppCompatActivity;

import cat.itb.list_grid_view.R;

public class CheckProfileActivity extends AppCompatActivity {
    Button save;
    EditText editName;
    SeekBar low, mid, high;
    int lowValue = 0, midValue = 0, highValue = 0;
    String name;
    int position;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            lowValue = bundle.getInt("low");
            midValue = bundle.getInt("mid");
            highValue = bundle.getInt("high");
            name = bundle.getString("name");
            position = bundle.getInt("position");
        }

        editName = findViewById(R.id.profileNameView);

        low = findViewById(R.id.lowBar);
        mid = findViewById(R.id.midBar);
        high = findViewById(R.id.highBar);

        save = findViewById(R.id.saveButton);

        editName.setText(name);
        low.setProgress(lowValue);
        mid.setProgress(midValue);
        high.setProgress(highValue);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CheckProfileActivity.this, MainActivity.class);
                intent.putExtra("name", editName.getText());
                intent.putExtra("low", low.getProgress());
                intent.putExtra("mid", mid.getProgress());
                intent.putExtra("high", high.getProgress());
                intent.putExtra("position", position);
                intent.putExtra("icon", 5);
                startActivity(intent);
            }
        });
    }
}
