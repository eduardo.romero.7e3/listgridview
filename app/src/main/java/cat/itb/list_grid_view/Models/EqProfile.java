package cat.itb.list_grid_view.Models;

public class EqProfile {
    private int icon;
    private int lowPass;
    private int midPass;
    private int highPass;
    private String name;

    public EqProfile(int icon, int lowPass, int midPass, int highPass, String name) {
        this.icon = icon;
        this.lowPass = lowPass;
        this.midPass = midPass;
        this.highPass = highPass;
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getLowPass() {
        return lowPass;
    }

    public void setLowPass(int lowPass) {
        this.lowPass = lowPass;
    }

    public int getMidPass() {
        return midPass;
    }

    public void setMidPass(int midPass) {
        this.midPass = midPass;
    }

    public int getHighPass() {
        return highPass;
    }

    public void setHighPass(int highPass) {
        this.highPass = highPass;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }
}
