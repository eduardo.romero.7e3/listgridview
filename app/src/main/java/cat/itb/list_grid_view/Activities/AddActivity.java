package cat.itb.list_grid_view.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import cat.itb.list_grid_view.R;

public class AddActivity extends AppCompatActivity {

    Button save;
    EditText editName;
    SeekBar low, mid, high;
    Spinner spinner;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        editName = findViewById(R.id.profileNameView);
        spinner = findViewById(R.id.iconSpinner);

        low = findViewById(R.id.lowBar);
        mid = findViewById(R.id.midBar);
        high = findViewById(R.id.highBar);

         save = findViewById(R.id.saveButton);

         save.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Intent intent = new Intent(AddActivity.this, MainActivity.class);
                 intent.putExtra("name", editName.getText());
                 intent.putExtra("low", low.getProgress());
                 intent.putExtra("mid", mid.getProgress());
                 intent.putExtra("high", high.getProgress());
                 intent.putExtra("icon", spinner.getSelectedItemPosition());
                 startActivity(intent);
             }
         });
    }


}


