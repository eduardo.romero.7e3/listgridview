package cat.itb.list_grid_view.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import cat.itb.list_grid_view.Models.EqProfile;
import cat.itb.list_grid_view.R;

public class MyAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private List<EqProfile> profileList;

    public MyAdapter(Context context, int layout, ArrayList<EqProfile> profileList) {
        this.context = context;
        this.layout = layout;
        this.profileList = profileList;
    }

    @Override
    public int getCount() {
        return profileList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.profileList.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(this.context);
            convertView = inflater.inflate(this.layout, null);
            holder = new ViewHolder();
            holder.nameTextView = convertView.findViewById(R.id.textView);
            holder.iconImageView = convertView.findViewById(R.id.icon);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }
        EqProfile profile = (EqProfile) getItem(position);
        String currentName = profile.toString();
        int currentIcon = profile.getIcon();

        holder.nameTextView.setText(currentName);
        holder.iconImageView.setImageResource(currentIcon);
        return convertView;
    }

    public class ViewHolder{
        private TextView nameTextView;
        private ImageView iconImageView;
    }
}
